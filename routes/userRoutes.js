const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    try {
        let resultSearching = UserService.getAll();
        if(resultSearching){
            res.send(resultSearching);
        }
        else {
            throw Error("Can't get users list");
        }
    }
    catch (error) {
        res.status(404).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const search = {id: req.params.id};
        let resultSearching = UserService.getOneById(search);
        if(resultSearching){
            res.locals.resultQuery = resultSearching;
        }
        else{
            throw Error("User not found");
        }
    }
    catch (error) {
        res.status(404).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    let user = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phoneNumber: req.body.phoneNumber,
        email: req.body.email,
        password: req.body.password
    }
    try {
        let resultSearching = UserService.search(user); //res.locals.user
        if(resultSearching){
            throw Error("User has already been in database");
        }

        let resultnNotUniqueEmail = UserService.getOneByEmail({email: user.email});
        if (resultnNotUniqueEmail){
            throw Error("User with this email has already been in database");
        }

        let resultnNotUniquePhoneNumber = UserService.getOneByPhoneTelephone({phoneNumber: user.phoneNumber});
        if (resultnNotUniquePhoneNumber){
            throw Error("User with this phone number has already been in database");
        }
        
        let resultAdding = UserService.add(user);
        if (!resultAdding){
            throw Error("User can't be added to database");
        }        
        res.locals.resultQuery = resultAdding;
        

    } catch (error) {
        res.status(400).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try{
        let updatedUser = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            phoneNumber: req.body.phoneNumber,
            email: req.body.email,
            password: req.body.password
        }

        let id = req.params.id;
        const search = {id};
        let resultSearching = UserService.getOneById(search);
        if(!resultSearching){
            throw Error("User not found");
        }

        const originEmail = resultSearching.email;
        if(originEmail != updatedUser.email){
            let resultnNotUniqueEmail = UserService.getOneByEmail({email: updatedUser.email});
            if (resultnNotUniqueEmail){
                throw Error("User with this email has already been in database");
            }
        }

        const originPhoneNumber = resultSearching.phoneNumber;
        if(originPhoneNumber != updatedUser.phoneNumber){
            let resultnNotUniquePhoneNumber = UserService.getOneByPhoneTelephone({phoneNumber: updatedUser.phoneNumber});
            if (resultnNotUniquePhoneNumber){
                throw Error("User with this phone number has already been in database");
            }
        }

        let resultUpdating = UserService.update(id, updatedUser); //res.locals.user
        if (!resultUpdating){
            throw Error("User can't be updated in database");
        }
        
        res.locals.resultQuery = resultUpdating;
        
    }
    catch (error) {
        res.status(400).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try{
        id = req.params.id;
        const search = {id};
        let resultSearching = UserService.getOneById(search);
        if(!resultSearching){
            throw Error("User not found");
        }
        
        let resultDeleting = UserService.delete(id);
        if(!resultDeleting){
            throw Error("User can't be updated in database");
        }
            
        res.locals.resultQuery = resultDeleting; 
    }
    catch(error){
        res.status(404).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;