const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    try {
        let resultSearching = FighterService.getAll();
        if(resultSearching){
            res.locals.resultQuery = resultSearching;
        }
        else {
            throw Error("Can't get fighers list");
        }
    } catch (err) {
        res.status(404).json({
            error: true,
            message: err.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try{
        const search = {id: req.params.id};
        let resultSearching = FighterService.getOneById(search);
        if(resultSearching){
            res.locals.resultQuery = resultSearching;
        }
        else {
            throw Error("Fighter not found");
        }
    }
    catch (error) {
        res.status(404).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    try {
        let fighter = {
            name: req.body.name,
            power: parseInt(req.body.power),
            health: 100,
            defense: parseInt(req.body.defense)
        }
        let resultSearching = FighterService.search(fighter); //res.locals.fighter
        if(resultSearching){
            throw Error("Fighter has already been in database");
        }
        let resultnNotUniqueName = FighterService.getOneByName({name: fighter.name});
        if (resultnNotUniqueName){
            throw Error("Fighter with this name has already been in database");
        }
            
        let resultAdding = FighterService.add(fighter); //res.locals.fighter
        if(!resultAdding){
            throw Error("Fighter can't be added to database");
        }

        res.locals.resultQuery = resultAdding;
    } catch (err) {
        res.status(400).json({
            error: true,
            message: err.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try{
        let updatedFighter = {
            name: req.body.name,
            power: parseInt(req.body.power),
            health: 100,
            defense: parseInt(req.body.defense)
        }
        let id = req.params.id;
        const search = {id};
        let resultSearching = FighterService.getOneById(search);
        if(!resultSearching){
            throw Error("Fighter not found");
        }

        let originName = resultSearching.name;
        if(originName != updatedFighter.name){
            let resultnNotUniqueName = FighterService.getOneByName({name: updatedFighter.name});
            if (resultnNotUniqueName){
                throw Error("Fighter with this name has already been in database");
            }
        }
       
        let resultUpdating = FighterService.update(id, updatedFighter);
        if (!resultUpdating){
            throw Error("Fighter can't be updated in database");
        }
        
        res.locals.resultQuery = resultUpdating;
    }
    catch (error) {
        res.status(400).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try{
        let id = req.params.id;
        const search = {id};
        let resultSearching = FighterService.getOneById(search);
        if(!resultSearching){
            throw Error("Fighter not found");
        }
        
        let resultDeleting = FighterService.delete(id);
        if(!resultDeleting){
            throw Error("Fighter can't be updated in database");
        }
        
        res.locals.resultQuery = resultDeleting;
    }
    catch(error){
        res.status(404).json({
            error: true,
            message: error.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;