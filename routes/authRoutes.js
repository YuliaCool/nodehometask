const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { loginUserValid } = require('../middlewares/user.validation.middleware');

const router = Router();

router.post('/login', loginUserValid, (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        loginInfo = {
            email: req.body.email,
            password: req.body.password
        }
        let resultSearching = AuthService.login(loginInfo);
        if(resultSearching){
            res.locals.resultQuery = resultSearching;
        }
        else {
            throw Error("Incorrect email or password");
        }
    } catch (err) {
        //res.err = err;
        res.status(400).json({
            error: true,
            message: err.message
        });
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;