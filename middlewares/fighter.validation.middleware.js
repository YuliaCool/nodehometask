const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {   
        fighter.name = req.body.name;
        fighter.power = parseInt(req.body.power);
        fighter.health = 100;
        fighter.defense = parseInt(req.body.defense);
        const extraFields = Object.keys(req.body).length - (Object.keys(fighter).length - 1);

        if(extraFields > 0){
            throw Error("Fighter entity to create has additional field");
        }
        if(
            !fighter.name || 
            !fighter.power || 
            !fighter.defense
            ){
            throw Error("Fighter entity to create has empty fields");
        }
        if (
            fighter.power >= 100 ||
            fighter.power < 0
            ){
            throw Error("Incorrect value of power field");
        }
        if (
            fighter.defense >= 10 ||
            fighter.defense < 0
            ){
            throw Error("Incorrect value of defence field");
        }
        // Is it better approach?
        // res.locals.fighter = fighter;
        next();
    }
    catch(err) {
        res.status(400).json({
            error: true,
            message: err.message
          });
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        fighter.name = req.body.name;
        fighter.power = parseInt(req.body.power);
        fighter.health = 100;
        fighter.defense = parseInt(req.body.defense);
        const extraFields = Object.keys(req.body).length - (Object.keys(fighter).length - 1);

        if(extraFields){
            throw Error("Fight entity to update has additional field");
        }
        if(
            !fighter.name || 
            !fighter.power || 
            !fighter.defense
            ){
            throw Error("Fighter entity to update has empty fields");
        }
        if (
            fighter.power >= 100 ||
            fighter.power < 0
            ){
            throw Error("Incorrect value of power field");
        }
        if (
            fighter.defense >= 10 ||
            fighter.defense < 0
            ){
                throw Error("Incorrect value of defence field");
        }
        // Is it better approach?
        // res.locals.updatedFighter = fighter;

        next();
    }
    catch(err) {
        res.status(400).json({
            error: true,
            message: err.message
          });
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;