const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try{
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.phoneNumber = req.body.phoneNumber;
        user.email = req.body.email;
        user.password = req.body.password;
        const extraFields = Object.keys(req.body).length - (Object.keys(user).length - 1);

        if(extraFields){
            throw Error("User entity to create has additional field");
        }
        if(
            !user.firstName || 
            !user.lastName ||
            !user.phoneNumber ||
            !user.email ||
            !user.password
            ) {
                throw Error("User entity to create has empty fields");
        }
        if(!validateEmail(user.email)){
            throw Error("User entity to create has incorrect email");
        }
        if(!validatePhoneNumber(user.phoneNumber)){
            throw Error("User entity to create has incorrect phone number");
        }
        if(!validatePassword(user.password)){
            throw Error("User entity to create has password which less than 3 symbols");
        }
        
        // Is it better approach?
        // res.locals.user = user;
        next();
        
    }
    catch(err) {
        res.status(400).json({
            error: true,
            message: err.message
          });
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try{
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.phoneNumber = req.body.phoneNumber;
        user.email = req.body.email;
        user.password = req.body.password;
        const extraFields = Object.keys(req.body).length - (Object.keys(user).length - 1);

        if(extraFields){
            throw Error("User entity to update has additional field");
        }
        if(
             !user.firstName || 
             !user.lastName ||
             !user.phoneNumber ||
             !user.email ||
             !user.password
             ) {
                throw Error("User entity to update has empty fields");
         }
        if(!validateEmail(user.email)){
            throw "User entity to update has incorrect email";
        }
        if(!validatePhoneNumber(user.phoneNumber)){
            throw Error("User entity to update has incorrect phone number");
        }
        if(!validatePassword(user.password)){
            throw Error("User entity to update has password which less than 3 symbols");
        }
        
        // Is it better approach?
        // res.locals.updatedUser = user;
        next();
        
    }
    catch(err) {
        res.status(400).json({
            error: true,
            message: err.message
          });
    }
}

const loginUserValid = (req, res, next) => {
    // I have added this function for checking if user enter valid data into "Sign in" window
    try {
        user.email = req.body.email;
        user.password = req.body.password;

        if(
            !user.password ||
            !user.email
        ){
            throw Error("User entity to login has empty fields");
        }
        if(!validateEmail(user.email)){
            throw Error("User entity to login has incorrect email");
        }
        if(!validatePassword(user.password)){
            throw Error("User entity to login has password which less than 3 symbols");
        }
        
        next();
    }
    catch(err){
        res.status(400).json({
            error: true,
            message: err.message
          });
    }
}

function validateEmail(email){
    const emailPattern = /^\w+([\.-]?\w+)*@gmail+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
}
function validatePhoneNumber(phoneNumber){
    const phoneNumberPattern = /\+380\d{9}/;
    return phoneNumberPattern.test(phoneNumber);
}
function validatePassword(password){
    const minLength = 3;
    return (password.length >= minLength);
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.loginUserValid = loginUserValid;