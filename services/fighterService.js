const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        const fighters = FighterRepository.getAll();
        if(!fighters){
            return null;
        }
        return fighters;
    }

    search(search) {
        let item = FighterRepository.getOne(search);
        if(!item){
            return null;
        }
        return item;
    }

    add(item) {
        const success = FighterRepository.create(item);
        if(!success){
            return null;
        }
        return success;
    }

    getOneById(id){
        const success = FighterRepository.getOne(id);
        if(!success){
            return null;
        }
        return success;
    }

    getOneByName(name){
        const success = FighterRepository.getOne(name);
        if(!success){
            return null;
        }
        return success;
    }

    update(id, updateData) {
        const item = FighterRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const updated = FighterRepository.update(id, updateData);
            if(!updated) {
                return null;
            }
            return updated;
        }   
    }

    delete(id){
        const item = FighterRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const deleted = FighterRepository.delete(id)
            if(!deleted) {
                return null;
            }
            return deleted;
        } 
    }

}

module.exports = new FighterService();