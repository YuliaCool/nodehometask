const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    add(item) {
        const success = UserRepository.create(item);
        if(!success){
            return null;
        }
        return success;
    }

    getAll(){
        const users = UserRepository.getAll();
        if(!users){
            return null;
        }
        return users;
    }

    getOneById(id){
        const success = UserRepository.getOne(id);
        if(!success){
            return null;
        }
        return success;
    }

    getOneByPhoneTelephone(number){
        const success = UserRepository.getOne(number);
        if(!success){
            return null;
        }
        return success;
    }

    getOneByEmail(email){
        const success = UserRepository.getOne(email);
        if(!success){
            return null;
        }
        return success;
    }

    update(id, updateData) {
        const item = UserRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const updated = UserRepository.update(id, updateData);
            if(!updated) {
                return null;
            }
            return updated;
        } 
    }

    delete(id){
        const item = UserRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const deleted = UserRepository.delete(id)
            if(!deleted) {
                return null;
            }
            return deleted;
        } 
    }
}

module.exports = new UserService();